package in.vayana.blog.gradle_demo
import com.weiglewilczek.slf4s.Logger

object GradleDemo {
  val logger = Logger("in.vayana.blog.gradle_demo")
  def foo() = {
    logger.debug("foo invoked.")
    "bar"
  }
  def main(args: Array[String]): Unit = {
    args foreach { arg =>
      logger.info("Hello %s!".format(arg))
    }
  }
}